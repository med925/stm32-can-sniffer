#include "stm32f1xx_hal.h"
#include "led.h"
#include "can.h"

#define NB_MSG  100


#define SLCAN_MTU 30// (sizeof("T1111222281122334455667788EA5F\r")+1)

#define SLCAN_STD_ID_LEN 3
#define SLCAN_EXT_ID_LEN 8


typedef struct{
  uint8_t start;
  uint8_t end;
  uint32_t data;
}CanFieldS;

typedef struct{
  uint32_t id;
  uint8_t changing;
  uint32_t freq;
  uint32_t lastTime;
  uint8_t lastData[8]/*,prevData[8]*/;
  
  //decompostion
  //CanFieldS field[4];
}CanSnifferPgnS;

typedef struct{
  ledStruct leds[2];
  CanSnifferPgnS pgns[NB_MSG];
  uint8_t snifferCursor;
  __IO uint32_t msCounter;
  __IO uint8_t tick10ms,tick100ms,tick1s;
}CanSnifferStruct;


void CANS_init(CanSnifferStruct * cans);
void CANS_Routine(CanSnifferStruct * cans);