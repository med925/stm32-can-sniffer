#include "stm32f1xx_hal.h"

typedef struct{
  CAN_RxHeaderTypeDef header;
  uint8_t Data[8];
}CanRxMsg;

typedef struct{
  CAN_TxHeaderTypeDef header;
  uint8_t Data[8];
}CanTxMsg;
void CAN_R();
void CAN_T(uint32_t id,uint8_t * Data,uint8_t length);
uint8_t CAN_IsReceived();
void CAN_ResetReceived();
uint8_t* CAN_GetRxData();
uint16_t CAN_GetRxId();
void CAN_SetFilter(uint16_t filter);
void CAN_Enable(uint8_t rate);
void CAN_Disable();