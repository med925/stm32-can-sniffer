/******************** (C) COPYRIGHT 2014 ACI ********************
* File Name          : fms.c
* Author             : Mohamed MASMOUDI
* Version            : V1.0
* Date               : 04/2014
* Description        : CAN LIB for STM32
****************************************************************************/

/* -------- Includes ------------------------------------------------------------------*/
#include "can.h"
#include "stdlib.h"
#include <string.h>
/* -------- Used Global variables -----------------------------------------------------*/
/* -------- Create Global variables ---------------------------------------------------*/
/* -------- Private define ------------------------------------------------------------*/
// CAN BAUD RATE SELECTION
//#define   _125KB
#define   _250KB
//#define  _500KB
//#define   _1000KB
#define USE_GPIOB
//#define USE_GPIOA
//#define USE_GPIOD

/* -------- Private variables ---------------------------------------------------------*/
extern CAN_HandleTypeDef hcan;
__IO CanRxMsg RxMessage;
__IO uint8_t CAN_DataReceived=0;
/* -------- Prototyp local ------------------------------------------------------------*/

/* -------- Code ----------------------------------------------------------------------*/
/*******************************************************************************
* Function Name  : CAN_R
* Description    : .
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void CAN_R(){
  if(!CAN_DataReceived){
            
      RxMessage.header.StdId=0x00;
      RxMessage.header.ExtId=0xFF;
      RxMessage.header.IDE=0;
      RxMessage.header.DLC=0;
      RxMessage.header.FilterMatchIndex=0;
      RxMessage.Data[0]=0x00;
      RxMessage.Data[1]=0x00;
      RxMessage.Data[2]=0x00;
      RxMessage.Data[3]=0x00;
      RxMessage.Data[4]=0x00;
      RxMessage.Data[5]=0x00;
      RxMessage.Data[6]=0x00;
      RxMessage.Data[7]=0x00;

      HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO1, (CAN_RxHeaderTypeDef *)&RxMessage.header,(uint8_t *)RxMessage.Data);
      CAN_DataReceived=1;
  }else {
    CanRxMsg junk;
    HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO1, (CAN_RxHeaderTypeDef *)&junk.header,(uint8_t *)junk.Data);
  }
}
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){
    CAN_R();
}
/*******************************************************************************
* Function Name  : CAN_T
* Description    : .
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void CAN_T(uint32_t id,uint8_t * Data,uint8_t length){
  uint32_t mailBox=0;
  CanTxMsg TxMessage;
  /* transmit 1 message */
  TxMessage.header.StdId=0x00;
  TxMessage.header.ExtId=id*256;
  TxMessage.header.IDE=CAN_ID_EXT;
  TxMessage.header.RTR=CAN_RTR_DATA;
  TxMessage.header.DLC=length;
  memcpy(TxMessage.Data,Data,length);

  HAL_CAN_AddTxMessage(&hcan, &TxMessage.header,TxMessage.Data,&mailBox);
}
/*******************************************************************************
* Function Name  : CAN_IsReceived
* Description    : .
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint8_t CAN_IsReceived(){
    return CAN_DataReceived;
}
/*******************************************************************************
* Function Name  : CAN_ResetReceived
* Description    : .
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void CAN_ResetReceived(){
    CAN_DataReceived=0;
}
/*******************************************************************************
* Function Name  : CAN_GetRxData
* Description    : .
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint8_t* CAN_GetRxData(){
    return (uint8_t *)RxMessage.Data;
}
uint16_t CAN_GetRxId(){
    return (RxMessage.header.ExtId>>8)&0xFFFF;

}
__IO uint32_t mask;
void CAN_SetFilter(uint16_t filter){
   mask=~((uint32_t)filter);
   CAN_FilterTypeDef  CAN_FilterInitStructure;
  /* CAN filter init */
  CAN_FilterInitStructure.FilterMode=CAN_FILTERMODE_IDMASK;
  CAN_FilterInitStructure.FilterScale=CAN_FILTERSCALE_32BIT;
  CAN_FilterInitStructure.FilterIdHigh=0x0;
  CAN_FilterInitStructure.FilterIdLow=0x0;
  CAN_FilterInitStructure.FilterMaskIdHigh=0x0000;
  CAN_FilterInitStructure.FilterMaskIdLow=0x0000;
  CAN_FilterInitStructure.FilterFIFOAssignment=CAN_FILTER_FIFO1;
  CAN_FilterInitStructure.FilterActivation=ENABLE;
  CAN_FilterInitStructure.FilterBank=0;
  HAL_CAN_ConfigFilter(&hcan,&CAN_FilterInitStructure);
}

/******************* (C) COPYRIGHT 2014 ACI ********END OF FILE****/

