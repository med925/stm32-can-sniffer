#include "can_sniffer.h"

extern __IO CanRxMsg RxMessage;
extern CAN_HandleTypeDef hcan;
void CANS_init(CanSnifferStruct * cans){
  cans->snifferCursor=0;
  CAN_SetFilter(0xFFFFFFFF);
  HAL_CAN_Start(&hcan);
  HAL_CAN_ActivateNotification(&hcan,CAN_IT_RX_FIFO1_MSG_PENDING);
}
int8_t slcan_parse_frame(uint8_t *buf, CanRxMsg *frame) {
    uint8_t i = 0;
    uint8_t id_len, j;
    uint32_t tmp;

    for (j=0; j < SLCAN_MTU; j++) {
        buf[j] = '\0';
    }

    // add character for frame type
    if (frame->header.RTR == CAN_RTR_DATA) {
        buf[i] = 't';
    } else if (frame->header.RTR == CAN_RTR_REMOTE) {
        buf[i] = 'r';
    }

    // assume standard identifier
    id_len = SLCAN_STD_ID_LEN;
    tmp = frame->header.StdId;
    // check if extended
    if (frame->header.IDE == CAN_ID_EXT) {
        // convert first char to upper case for extended frame
        buf[i] -= 32;
        id_len = SLCAN_EXT_ID_LEN;
        tmp = frame->header.ExtId;
    }
    i++;

    // add identifier to buffer
    for(j=id_len; j > 0; j--) {
        // add nybble to buffer
        buf[j] = (tmp & 0xF);
        tmp = tmp >> 4;
        i++;
    }

    // add DLC to buffer
    buf[i++] = frame->header.DLC;

    // add data bytes
    for (j = 0; j < frame->header.DLC; j++) {
        buf[i++] = (frame->Data[j] >> 4);
        buf[i++] = (frame->Data[j] & 0x0F);
    }

    // convert to ASCII (2nd character to end)
    for (j = 1; j < i; j++) {
        if (buf[j] < 0xA) {
            buf[j] += 0x30;
        } else {
            buf[j] += 0x37;
        }
    }

    // add carrage return (slcan EOL)
    buf[i++] = '\r';

    // return number of bytes in string
    return i;
}
void CANS_Routine(CanSnifferStruct * cans){

  if(CAN_IsReceived()){
#ifdef ALPHA_PARSER
      uint8_t alreadyRec=0;
      /*if(alphaSys.trt.EnableDiag){
        sprintf(message,"%X\t%X\t%X\t%X %X %X %X %X %X %X %X\n\r", 
                RxMessage.ExtId,
                RxMessage.DLC,
                RxMessage.StdId,
                RxMessage.Data[0],
                RxMessage.Data[1],
                RxMessage.Data[2],
                RxMessage.Data[3],
                RxMessage.Data[4],
                RxMessage.Data[5],
                RxMessage.Data[6],
                RxMessage.Data[7]);
        MACHINE_UartDebug(message);
      }*/
      for(int i=0;i<cans->snifferCursor;i++){
        if(cans->pgns[i].id==RxMessage.header.ExtId){
          //already received
          cans->pgns[i].freq=HAL_GetTick()-cans->pgns[i].freq;
          if(memcmp(cans->pgns[i].lastData,(uint8_t *)RxMessage.Data,8)!=0){
            cans->pgns[i].changing=1;
            //memcpy(sniffer[i].prevData,sniffer[i].lastData,8);
            memcpy(cans->pgns[i].lastData,(uint8_t *)RxMessage.Data,8);
          }
          alreadyRec=1;
        }
      }
      if(!alreadyRec&&cans->snifferCursor<NB_MSG){
        //Receiving New Frame
        cans->pgns[cans->snifferCursor].id=RxMessage.header.ExtId;
        memcpy(cans->pgns[cans->snifferCursor].lastData,(uint8_t *)RxMessage.Data,8);
        //memcpy(cans->pgns[snifferCursor].prevData,(uint8_t *)RxMessage.Data,8);
        cans->pgns[cans->snifferCursor].freq=0;
        cans->pgns[cans->snifferCursor].lastTime=HAL_GetTick();
        cans->pgns[cans->snifferCursor].changing=0;
        cans->snifferCursor++;
      }
#endif
#ifdef SLCAN
      uint8_t msg_buf[SLCAN_MTU];
      slcan_parse_frame((uint8_t *)&msg_buf, (CanRxMsg*)&RxMessage);
      MACHINE_UartDebug(msg_buf);
#endif
      CAN_ResetReceived();
    }
}

void CANS_Display(CanSnifferStruct * cans){    
  char message[128];
  MACHINE_UartDebugChar(27);
  MACHINE_UartDebug("[H");     // cursor to home command 
  for(int i=0;i<cans->snifferCursor;i++){
    sprintf(message,"%08X     %02X     %08X    %02X %02X %02X %02X %02X %02X %02X %02X      \n\r", 
            cans->pgns[i].id,
            cans->pgns[i].changing,
            cans->pgns[i].freq,
            cans->pgns[i].lastData[0],
            cans->pgns[i].lastData[1],
            cans->pgns[i].lastData[2],
            cans->pgns[i].lastData[3],
            cans->pgns[i].lastData[4],
            cans->pgns[i].lastData[5],
            cans->pgns[i].lastData[6],
            cans->pgns[i].lastData[7]);
    MACHINE_UartDebug(message);
  }
}

CanSnifferPgnS PingPgns[NB_MSG]={

0x743,0,0,0,02,0x21,0x01,0xFF,0xFF,0xFF,0xFF,0xFF,
//0x0CFE6C55,01,0x100E,0,0xAC,0xD6,0xE7,0x6E,0xFF,0xFF,0x00,0x28
};
void CANS_ping(){
    CanTxMsg msg;
    uint32_t mailBox=0;
    memcpy(msg.Data,PingPgns[0].lastData,8);
    msg.header.ExtId=PingPgns[0].id;
    msg.header.DLC=8;
    msg.header.IDE=CAN_ID_STD;
    msg.header.StdId=PingPgns[0].id;
    msg.header.RTR=CAN_RTR_DATA;
    msg.header.TransmitGlobalTime=0;

    HAL_CAN_AddTxMessage(&hcan, &msg.header,msg.Data,&mailBox);
}
uint32_t timing=0x3FFFFF;
uint16_t ids[]={0x743,0x763};

//uint32_t ids[]={0x14FF5F40};
void CANS_DisplayFilter(){   
  
  char message[128];
  timing --;
  /*if(!timing){
    CANS_ping();
    timing=0x3FFFFF;
  }*/
  if(CAN_IsReceived()){
    for(int i=0;i<2;i++){
      if(RxMessage.header.StdId==ids[i]){
        sprintf(message,"%08X  %02X   %02X %02X %02X %02X %02X %02X %02X %02X      \n\r", 
        RxMessage.header.StdId,
        RxMessage.header.DLC,
        RxMessage.Data[0],
        RxMessage.Data[1],
        RxMessage.Data[2],
        RxMessage.Data[3],
        RxMessage.Data[4],
        RxMessage.Data[5],
        RxMessage.Data[6],
        RxMessage.Data[7]);
        MACHINE_UartDebug(message);
      }
    }
    CAN_ResetReceived();
  }
    
}

CanSnifferPgnS EmulationPgns[NB_MSG]={
//0x18FEF500,01,0x06A4,0,0x28,0x82,0x58,0xFF,0xFF,0xFF,0xFF,0xFF,

0x18FEC130,01,0x0FA0,0,0,0,0,0x30,0,0x00,0,0,
//0x0CFE6C55,01,0x100E,0,0xAC,0xD6,0xE7,0x6E,0xFF,0xFF,0x00,0x28,
//0x18FEF555,01,0x0FA0,0,0xFF,0xFF,0xFF,0x2D,0x27,0xFF,0xFF,0xFF,
//0x18FEF255,01,0x06A6,0,0x58,0x02,0x01,0x50,0xFF,0xFF,0xFF,0xFF,
//0x18FDA455,01,0x06A4,0,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
//0x18FE4E56,01,0x06A4,0,0x93,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
//0x18FDA556,01,0x06A4,0,0xEF,0xBD,0xBD,0xF7,0xFF,0xFF,0xFF,0xFF,
//0x18FE5856,01,0x06A4,0,0x1E,0x00,0x28,0x00,0x32,0x00,0x3C,0x00,
//0x18F00056,01,0x06A4,0,0xF3,0xD5,0xFF,0xFF,0xFF,0xFF,0x6E,0xFF,
//0x18FF1255,01,0x0FA0,0,0x40,0x19,0x01,0x00,0xE0,0xA5,0x01,0x00,
//0x18FF2255,01,0x06A4,0,0xE0,0xA5,0x01,0x00,0x82,0x8C,0x96,0x00,
//0x18FF4555,01,0x06A4,0,0xFB,0x3E,0x65,0x49,0x8C,0xFF,0xFF,0xFF,
//0x18FEF155,01,0x19F1,0,0xF7,0x00,0x1E,0xDE,0xFF,0xFF,0xE3,0xFF,
//0x0CF00355,01,0x1003,0,0xFF,0x82,0x58,0xFF,0xFF,0xFF,0xFF,0xFF,
//0x0CF00455,01,0x1006,0,0xFF,0xFF,0xA9,0xC0,0x5D,0xFF,0xFF,0xFF,
//0x18FEEE01,00,0x0000,0,0x6E,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
//0x18F00555,01,0x1006,0,0x82,0xFF,0xFF,0x83,0xFF,0xFF,0xFF,0xFF,
//0x18FF1101,00,0x0000,0,0x40,0x19,0x01,0x00,0xE0,0xA5,0x01,0x00,
//0x18FF2A55,01,0x06CD,0,0xD0,0x84,0x70,0x17,0x18,0x60,0xAF,0xFF,
//0x18FF1001,00,0x0000,0,0xE0,0xA5,0x01,0x00,0xA0,0x8C,0x00,0x00
};
void CANS_emulate(){
  for(int i=0;i<sizeof(EmulationPgns)/sizeof(CanSnifferPgnS)&&EmulationPgns[i].id!=0;i++){
    if(HAL_GetTick()>EmulationPgns[i].lastTime+EmulationPgns[i].freq){
      CanTxMsg msg;
      EmulationPgns[0].lastData[3]++;
      uint32_t mailBox=0;
      EmulationPgns[i].lastTime=HAL_GetTick();
      memcpy(msg.Data,EmulationPgns[i].lastData,8);
      msg.header.ExtId=EmulationPgns[i].id;
      msg.header.DLC=8;
      msg.header.IDE=CAN_ID_EXT;
      msg.header.StdId=0;
      msg.header.RTR=CAN_RTR_DATA;
      msg.header.TransmitGlobalTime=0;
      HAL_CAN_AddTxMessage(&hcan, &msg.header,msg.Data,&mailBox);
    }
  }
}

